http = require('http')
stdlib_url = require("url")

module.exports = class Recho

  @run: ({host, port, url}) ->
    new Recho({host, port}).run()

  constructor: ({@host, @port, @url}) ->
    @url ?= "http://#{@host}:#{@port}"
    if @url.slice(-1) == '/'
      @url = @url.slice(0, -1)

    @server = http.createServer (request, response) =>

      chunks = []
      request.on "data", (chunk) =>
        chunks.push chunk

      request.on "end", =>
        request.body = chunks.join()
        @serve(request, response)

  run: ->
    process.on "exit", -> console.error "Exiting Recho."
    @server.listen @port, @host
    console.log "Running Recho on http://#{@host}:#{@port}/ with public url: #{@url}"

  serve: (request, response) ->
    result = @dispatch(request, response)
    return if result == null
    {status, headers, content} = result
    if headers
      for key, value of headers
        response.setHeader key, value

    if content == null
      body = ""
    else
      body = JSON.stringify(content, null, 2)
      response.setHeader "Content-Type", "application/json"
      response.setHeader "Content-Length", Buffer.byteLength(body)

    response.writeHead status
    response.end body

  dispatch: (request, response) ->
    url = stdlib_url.parse(request.url)

    content =
      url: request.url
      method: request.method
      headers: {}
      body: request.body

    for key, value of request.headers
      content.headers[key.toLowerCase()] = value

    console.log "REQUEST:", content
    headers = {}
    try
      [_full, status, other] = url.pathname.match /\/(\d\d\d)|(.*)$/
      if status?
        switch status
          when "201"
            headers =
              "Location": "#{@url}/200"
          when "204"
            content = null
          when "301"
            headers =
              "Location": "#{@url}/200"
            content = null
          when "302"
            headers =
              "Location": "#{@url}/200"
            content: null
      else
        status = 404

    return {status, headers, content}

